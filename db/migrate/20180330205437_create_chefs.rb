class CreateChefs < ActiveRecord::Migration[5.1]
  def change
    create_table :chefs do |t|
      t.string :chefname , limit: 50, default: '', null: false
      t.string :email, limit: 50, default: '', null: false
      t.timestamps
    end
  end
end
