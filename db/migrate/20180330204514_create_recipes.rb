class CreateRecipes < ActiveRecord::Migration[5.1]
  def change
    create_table :recipes do |t|
      t.string  :name, limit: 50 , default: '', null: false
      t.text  :description, default: '', null: false
      t.timestamps
    end
  end
end
