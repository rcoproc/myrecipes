class CreateTodos < ActiveRecord::Migration[5.1]
  def change
    create_table :todos do |t|
      t.string :name, limit: 30, default: '', null: false
      t.text :description, default: '', null: false

      t.timestamps
    end
  end
end
